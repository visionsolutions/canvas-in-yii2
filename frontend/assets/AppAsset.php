<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'canvas/css/bootstrap.css',
        'canvas/style.css',
        'canvas/css/dark.css',
        'canvas/css/font-icons.css',
        'canvas/css/animate.css',
        'canvas/css/magnific-popup.css',
        'canvas/css/responsive.css',
    ];
    public $js = [
        'canvas/js/plugins.js',
        'canvas/js/functions.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
