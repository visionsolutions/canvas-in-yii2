<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Update Lofin form
 */
class LoginDetails extends \yii\db\ActiveRecord
{
    public $password;
    
    public static function tableName()
    {
       return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 3, 'max' => 255],
            ['username', 'checkUser'],

            ['email_address', 'filter', 'filter' => 'trim'],
            ['email_address', 'required'],
            ['email_address', 'email'],
            ['email_address', 'checkUser'],
           
            ['security_answer', 'required'],
            ['security_answer', 'string', 'min' => 3, 'max' => 255],
        ];
    }
    
    public function checkUser($attribute,$params){
        switch($attribute){
            case "username":
                if(\Yii::$app->user->identity->username!=$this->username){
                    $models = User::find()->where(['username' => $this->username])->one();
                    if(count($models)>0){
                        $this->addError($attribute, 'This username has already been taken.');
                    }
                }
            break;
            case "email_address":
                if(\Yii::$app->user->identity->email_address!=$this->email_address){
                    $models = User::find()->where(['email_address' => $this->email_address])->one();
                    if(count($models)>0){
                        $this->addError($attribute, 'This email address has already been taken.');
                    }
                }
            break;    
        }
    }
    
    /**
     * update user login information.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function update_login_information()
    {
        if ($this->validate()) {
            $post_data = Yii::$app->request->post();
            $password = $post_data['LoginDetails']['password'];
            $date = date('Y-m-d H:i:s');
            $user_id = \Yii::$app->user->identity->id;
            $user = User::findOne($user_id);
            $user->email_address = $this->email_address;
            $user->username = $this->username;
            if($password!=""){
              $user->password_hash = \Yii::$app->security->generatePasswordHash($password); 
            }
            $user->security_answer = $this->security_answer;
            $user->updated_at = $date;
            $user->update();
            return true;
        }

        return false;
    }
    
    /**
     * cancel user account.
     *
     */
    public function cancel_my_account()
    {
        $user_id = \Yii::$app->user->identity->id;
        $date = date('Y-m-d H:i:s');
        $user = User::findOne($user_id);
        $user->status = 0;
        $user->updated_at = $date;
        $user->update();
        return true;
        
    }
    

}
