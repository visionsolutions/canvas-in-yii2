<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class PaymentForm extends Model
{
    public $card_number;
    public $expiration_month;
    public $expiration_year;
    public $security_code;
    public $card_type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['card_number', 'expiration_month', 'expiration_year', 'security_code', 'card_type'], 'required'],
            ['card_number', 'string', 'min' => 12, 'max' => 16],
            ['security_code', 'string', 'min' => 3, 'max' => 4],
        ];
    }

    

    /**
     * Payment
     */
    
}
