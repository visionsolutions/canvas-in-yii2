<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;


/**
 * User Management form
 */
class UserManagement extends \yii\db\ActiveRecord
{
    public $password;
    
    public static function tableName()
    {
       return 'user';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 3, 'max' => 255],

            ['email_address', 'filter', 'filter' => 'trim'],
            ['email_address', 'required'],
            ['email_address', 'email'],
            ['email_address', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            
            ['password', 'required'],
            ['password', 'string', 'min' => 3, 'max' => 255],
            
            ['product_access', 'required'],
            
        ];
    }
    
    /**
     * save new user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function save_new_user()
    {
        if ($this->validate()) {
            $contact_type = \Yii::$app->params['products_access'][$this->product_access];
            $user_id = \Yii::$app->user->identity->id;
            $date = date('Y-m-d H:i:s');
            $user = new User();
            $user->name = $this->name;
            $user->email_address = $this->email_address;
            $user->username = $this->username;
            $user->setPassword($this->password);
            $user->contact_type = $contact_type;
            $user->is_payment_made = 'y';
            $user->main_user_id = $user_id;
            $user->product_access = $this->product_access;
            $user->generateAuthKey();
            $user->created_at = $date;
            $user->updated_at = $date;
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
 
    
}
