<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;


/**
 * User Management form
 */
class UserUpdateManagement extends \yii\db\ActiveRecord
{
    public $password;
    
    public static function tableName()
    {
       return 'user';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 3, 'max' => 255],

            ['email_address', 'filter', 'filter' => 'trim'],
            ['email_address', 'required'],
            ['email_address', 'email'],
            ['email_address', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            
            ['product_access', 'required'],
            
        ];
    }
    
 
    /**
     * update user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function update_user($id = NULL)
    {
        if ($this->validate() and $id != NULL) {
            $contact_type = \Yii::$app->params['products_access'][$this->product_access];
            $date = date('Y-m-d H:i:s');
            $user = User::find()->where(['id' => $id])->one();
            $user->name = $this->name;
            $user->email_address = $this->email_address;
            $user->username = $this->username;
            if($this->password != "") {
                $user->setPassword($this->password);
            }
            $user->contact_type = $contact_type;
            $user->product_access = $this->product_access;
            $user->updated_at = $date;
            if ($user->update()) {
                return $user;
            }
        }

        return null;
    }
    
}
