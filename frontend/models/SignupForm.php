<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $name;
    public $email_address;
    public $phone;
    public $company_name;
    public $password;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 3, 'max' => 255],

            ['email_address', 'filter', 'filter' => 'trim'],
            ['email_address', 'required'],
            ['email_address', 'email'],
            ['email_address', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['phone', 'required'],
            ['phone', 'string', 'min' => 3, 'max' => 255],
            
            ['company_name', 'required'],
            ['company_name', 'string', 'min' => 3, 'max' => 255],
        ];
    }
    
    
    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $products_access = \Yii::$app->params['products_access'];
            $date = date('Y-m-d H:i:s');
            $user = new User();
            $password = $this->randomPassword();
            Yii::$app->getSession()->set('user_custom_password', $password);
            $this->password = $password;
            $user->name = $this->name;
            $user->email_address = $this->email_address;
            $user->username = $this->email_address;
            $user->setPassword($this->password);
            $user->phone = $this->phone;
            $user->company_name = $this->company_name;
            $user->contact_type = $products_access[1];
            $user->generateAuthKey();
            $user->created_at = $date;
            $user->updated_at = $date;
            if ($user->save()) {
                return $user;
            }
        }

        return null;
    }
    
    public function update_user_status($paypal_response = array())
    {
        $paypal_response = serialize($paypal_response);
        $user_id = \Yii::$app->user->identity->id;
        $user = User::findOne($user_id);
        $user->is_payment_made = 'y';
        $user->paypal_response = $paypal_response;
        $user->update();
        
    }
    
    private function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

}
