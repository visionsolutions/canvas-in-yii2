<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class Membership extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
       return 'user';
    }
    
   
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['membership_status', 'filter', 'filter' => 'trim'],
            ['membership_status', 'required'],
        ];
    }
    
    public function update_membership_status()
    {
        if ($this->validate()) {
            $user_id = \Yii::$app->user->identity->id;
            $membership_status = \Yii::$app->user->identity->membership_status;
            if($membership_status != $this->membership_status){
                $date = date('Y-m-d H:i:s');
                $user = User::findOne($user_id);
                $user->membership_status = $this->membership_status;
                $user->updated_at = $date;
                $user->update();
                return true;
            }else {
                return false;
            }
        }        
    }
    
    
}
