<?php
namespace frontend\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class BillingHistory extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
       return 'billing_history';
    }
 
    public function update_billing_history($package_id = NULL){
        if($package_id != NULL) {
            $user_id = \Yii::$app->user->identity->id;
            $package = \Yii::$app->params['packages'][$package_id - 1];
            $billing_history = new BillingHistory();
            $date = date('Y-m-d H:i:s');
            $billing_history->type = 'payment';
            $billing_history->status = 'Close';
            $billing_history->amount = $package['package_price'];
            $billing_history->user_id = $user_id;
            $billing_history->created_at = $date;
            $billing_history->save();
        }
    }
    
}
