<?php
namespace frontend\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class Order extends \yii\db\ActiveRecord
{
    
    
    public static function tableName()
    {
       return 'order';
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            ['note', 'filter', 'filter' => 'trim'],
            ['note', 'required'],
            ['note', 'string', 'min' => 3, 'max' => 255],
            
            
        ];
    }
    
    
    public function save_user_order()
    {
        $package_id = Yii::$app->getRequest()->getQueryParam('id');
        $package = Yii::$app->params['packages'][$package_id - 1];
        $post_data = Yii::$app->request->post();
        
        $user_id = \Yii::$app->user->identity->id;
        $order = Order::find()->where(['user_id' => $user_id])->one();
        if ($order == NULL){
            $order = new Order();
        }
        
        $date = date('Y-m-d H:i:s');
        $order->package_id = $package_id;
        $order->package_name = $package['package_name'];
        $order->package_price = $package['package_price'];
        $order->note = $this->note;
        $order->user_id = $user_id;
        $order->is_active = 'n';
        $order->created_at = $date;
        $order->updated_at = $date;
        
        if ($order->save()) {
            return $order;
        }

        return null;
    }
    
    public function update_user_order()
    {
        $package_id = Yii::$app->getRequest()->getQueryParam('id');
        $package = Yii::$app->params['packages'][$package_id - 1];
        
        $user_id = \Yii::$app->user->identity->id;
        $order = Order::find()->where(['user_id' => $user_id])->one();
        if ($order == NULL){
            $order = new Order();
        }
        
        $date = date('Y-m-d H:i:s');
        $order->package_id = $package_id;
        $order->package_name = $package['package_name'];
        $order->package_price = $package['package_price'];
        $order->user_id = $user_id;
        $order->is_active = 'y';
        $order->created_at = $date;
        $order->updated_at = $date;
        
        if ($order->save()) {
            return $order;
        }

        return null;
    }

    
}
