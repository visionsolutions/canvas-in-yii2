<?php
namespace frontend\models;

use common\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class ContactInformation extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
       return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['contact_type', 'filter', 'filter' => 'trim'],
            ['contact_type', 'required'],
            ['contact_type', 'string', 'min' => 3, 'max' => 255],
            
            ['name', 'filter', 'filter' => 'trim'],
            ['name', 'required'],
            ['name', 'string', 'min' => 3, 'max' => 255],            

            ['email_address', 'filter', 'filter' => 'trim'],
            ['email_address', 'required'],
            ['email_address', 'email'],
            ['email_address', 'checkUser'],
            
            ['address', 'filter', 'filter' => 'trim'],
            ['address', 'required'],
            ['address', 'string', 'min' => 3, 'max' => 255],
            
            ['phone', 'filter', 'filter' => 'trim'],
            ['phone', 'required'],
            ['phone', 'string', 'min' => 3, 'max' => 255],
            
            ['time_zone', 'filter', 'filter' => 'trim'],
            ['time_zone', 'required'],
            ['time_zone', 'string', 'min' => 3, 'max' => 255],
        ];
    }
    
    public function checkUser($attribute,$params){
        switch($attribute){
            case "email_address":
                if(\Yii::$app->user->identity->email_address!=$this->email_address){
                    $models = User::find()->where(['email_address' => $this->email_address])->one();
                    if(count($models)>0){
                        $this->addError($attribute, 'This email address has already been taken.');
                    }
                }
            break;    
        }
    }
    
    /**
     * update user contact information.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function update_contact_information()
    {
        if ($this->validate()) {
            $date = date('Y-m-d H:i:s');
            $user_id = \Yii::$app->user->identity->id;
            $user = User::findOne($user_id);
            $user->contact_type = $this->contact_type;
            $user->name = $this->name;
            $user->email_address = $this->email_address;
            $user->address = $this->address;
            $user->phone = $this->phone;
            $user->time_zone = $this->time_zone;
            $user->updated_at = $date;
            $user->update();
            return true;
        }

        return false;
    }   
    

}
