<?php
namespace frontend\models;

use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class UserAddress extends \yii\db\ActiveRecord
{
    
    public static function tableName()
    {
       return 'user_address';
    }
    
   
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['billing_first_name', 'filter', 'filter' => 'trim'],
            ['billing_first_name', 'required'],
            ['billing_first_name', 'string', 'min' => 3, 'max' => 255],
            
            ['billing_last_name', 'filter', 'filter' => 'trim'],
            ['billing_last_name', 'required'],
            ['billing_last_name', 'string', 'min' => 3, 'max' => 255],
            
            ['billing_company_name', 'filter', 'filter' => 'trim'],
            ['billing_company_name', 'required'],
            ['billing_company_name', 'string', 'min' => 3, 'max' => 255],
            
            ['billing_vat_number', 'filter', 'filter' => 'trim'],
            ['billing_vat_number', 'required'],
            ['billing_vat_number', 'string', 'min' => 3, 'max' => 255],
            
            ['billing_address', 'filter', 'filter' => 'trim'],
            ['billing_address', 'required'],
            ['billing_address', 'string', 'min' => 3, 'max' => 255],
            
            ['billing_city', 'filter', 'filter' => 'trim'],
            ['billing_city', 'required'],
            ['billing_city', 'string', 'min' => 3, 'max' => 255],
            
            ['billing_country', 'filter', 'filter' => 'trim'],
            ['billing_country', 'required'],
            ['billing_country', 'string', 'min' => 2, 'max' => 255],

            ['billing_email_address', 'filter', 'filter' => 'trim'],
            ['billing_email_address', 'required'],
            ['billing_email_address', 'email'],
            
            ['billing_phone', 'filter', 'filter' => 'trim'],
            ['billing_phone', 'required'],
            ['billing_phone', 'string', 'min' => 3, 'max' => 255],
            
            ['shipping_first_name', 'filter', 'filter' => 'trim'],
            ['shipping_first_name', 'required'],
            ['shipping_first_name', 'string', 'min' => 3, 'max' => 255],
            
            ['shipping_last_name', 'filter', 'filter' => 'trim'],
            ['shipping_last_name', 'required'],
            ['shipping_last_name', 'string', 'min' => 3, 'max' => 255],
            
            ['shipping_company_name', 'filter', 'filter' => 'trim'],
            ['shipping_company_name', 'required'],
            ['shipping_company_name', 'string', 'min' => 3, 'max' => 255],
            
            ['shipping_vat_number', 'filter', 'filter' => 'trim'],
            ['shipping_vat_number', 'required'],
            ['shipping_vat_number', 'string', 'min' => 3, 'max' => 255],
            
            ['shipping_address', 'filter', 'filter' => 'trim'],
            ['shipping_address', 'required'],
            ['shipping_address', 'string', 'min' => 3, 'max' => 255],
            
            ['shipping_city', 'filter', 'filter' => 'trim'],
            ['shipping_city', 'required'],
            ['shipping_city', 'string', 'min' => 3, 'max' => 255],
            
            ['shipping_country', 'filter', 'filter' => 'trim'],
            ['shipping_country', 'required'],
            ['shipping_country', 'string', 'min' => 2, 'max' => 255],
            
            
        ];
    }
    
    public function save_user_address()
    {
        $user_id = \Yii::$app->user->identity->id;
        
        $user_address = UserAddress::find()->where(['user_id' => $user_id])->one();
        if ($user_address == NULL){
            $user_address = new UserAddress();
        }
        
        $date = date('Y-m-d H:i:s');
        $this->user_id = \Yii::$app->user->identity->id;
        $user_address->billing_first_name = $this->billing_first_name;
        $user_address->billing_last_name = $this->billing_last_name;
        $user_address->billing_company_name = $this->billing_company_name;
        $user_address->billing_vat_number = $this->billing_vat_number;
        $user_address->billing_address = $this->billing_address;
        $user_address->billing_city = $this->billing_city;
        $user_address->billing_country = $this->billing_country;
        $user_address->billing_email_address = $this->billing_email_address;
        $user_address->billing_phone = $this->billing_phone;
        $user_address->shipping_first_name = $this->shipping_first_name;
        $user_address->shipping_last_name = $this->shipping_last_name;
        $user_address->shipping_company_name = $this->shipping_company_name;
        $user_address->shipping_vat_number = $this->shipping_vat_number;
        $user_address->shipping_address = $this->shipping_address;
        $user_address->shipping_city = $this->shipping_city;
        $user_address->shipping_country = $this->shipping_country;
        $user_address->user_id = $this->user_id;
        $user_address->created_at = $date;
        $user_address->updated_at = $date;
        
        if ($user_address->save()) {
            return $user_address;
        }

        return null;
    }
    
    
}
