<?php
namespace frontend\controllers;

use Yii;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\LoginDetails;
use frontend\models\ContactInformation;
use common\models\Country;
use frontend\models\UserAddress;
use frontend\models\BillingHistory;
use frontend\models\UserManagement;
use frontend\models\UserUpdateManagement;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use kartik\mpdf\Pdf;
use frontend\models\Membership;

/**
 * Site controller
 */
class UserController extends Controller
{
    public $layout = "user";
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['register', 'login', 'logout', 'index', 'stats', 'settings', 'membership', 'cancel-my-account', 'login-details', 'contact-information', 'billing-and-payments', 'billing-history', 'user-management', 'create-new-user', 'update-user', 'download-pdf', 'request-password-reset', 'reset-password'],
                'rules' => [
                    [
                        'actions' => ['login', 'register', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout', 'index', 'stats', 'settings', 'membership', 'cancel-my-account', 'login-details', 'contact-information', 'billing-and-payments', 'billing-history', 'user-management', 'create-new-user', 'update-user', 'download-pdf'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        if($is_payment_made == 'n'){
            return $this->goHome();
        }
        
        $this->layout = "dashboard";
        return $this->render('index');
    }
    
    public function actionCamera()
    {
        
        $this->layout = "camera";
        return $this->render('camera');
    }
    
    public function actionStats()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        if($is_payment_made == 'n'){
            return $this->goHome();
        }
        
        $this->layout = "dashboard";
        return $this->render('stats');
    }
    
    public function actionSettings()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        if($is_payment_made == 'n'){
            return $this->goHome();
        }
        
        return $this->render('settings');
    }
    
    public function actionMembership()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        if($is_payment_made == 'n'){
            return $this->goHome();
        }
        
        $user_id = \Yii::$app->user->identity->id;
        $model = Membership::find()->where(['id' => $user_id])->one();
        
        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->update_membership_status()) {
                
                $paypal_response = \Yii::$app->user->identity->paypal_response;
                $paypal_response = unserialize($paypal_response);
                $paypal_profile_id = $paypal_response['paypal_response']['PROFILEID'];
                $paypal_profile_status = 'Suspend';
                $paypal_profile_status_reasonse = 'Profile is Suspend by User';
                
                $post_data = Yii::$app->request->post();
                $membership_status = $post_data['Membership']['membership_status'];
                if($membership_status == 'y'){  
                    $paypal_profile_status = 'Reactivate';
                    $paypal_profile_status_reasonse = 'Profile is Active by User';
                }
                \Yii::$app->mypaypal->manageProfileStatus($paypal_profile_id, $paypal_profile_status, $paypal_profile_status_reasonse);
                
                Yii::$app->getSession()->setFlash('success', 'Your membership status has been updated.');
            }else {
                Yii::$app->getSession()->setFlash('success', 'Your membership status has been updated.');
            }
        }
        
        return $this->render('membership', [ 'model' => $model]);
    }
    
    public function actionCancelMyAccount()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        if($is_payment_made == 'n'){
            return $this->goHome();
        }
        
        $paypal_response = \Yii::$app->user->identity->paypal_response;
        $paypal_response = unserialize($paypal_response);
        $paypal_profile_id = $paypal_response['paypal_response']['PROFILEID'];
        $paypal_profile_status = 'Cancel';
        $paypal_profile_status_reasonse = 'Account is cancel by User';
        //\Yii::$app->mypaypal->manageProfileStatus($paypal_profile_id, $paypal_profile_status, $paypal_profile_status_reasonse);
                
        $model = new LoginDetails();
        $model->cancel_my_account();
        return $this->render('cancel_my_account');
    }
    
    public function actionLoginDetails()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        if($is_payment_made == 'n'){
            return $this->goHome();
        }
        
        $user_id = \Yii::$app->user->identity->id;
        $model = LoginDetails::find()->where(['id' => $user_id])->one();
        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->update_login_information()) {
                Yii::$app->getSession()->setFlash('success', 'Your login information has been updated.');
            }
        }
        return $this->render('login_details', [ 'model' => $model ]);
    }
    
    public function actionContactInformation()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        if($is_payment_made == 'n'){
            return $this->goHome();
        }
        
        $user_id = \Yii::$app->user->identity->id;
        $model = ContactInformation::find()->where(['id' => $user_id])->one();
        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->update_contact_information()) {
                Yii::$app->getSession()->setFlash('success', 'Your contact information has been updated.');
            }
        }
        return $this->render('contact_information', [ 'model' => $model ]);
    }
    
    public function actionBillingAndPayments()
    {
        
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        $product_access = \Yii::$app->user->identity->product_access;
        if($is_payment_made == 'n' or $product_access != 1){
            return $this->goHome();
        }
        
        $user_id = \Yii::$app->user->identity->id;
        
        $model = UserAddress::find()->where(['user_id' => $user_id])->one();
        if ($model->load(Yii::$app->request->post())) { 
            if ($model->validate()) {
                if($model->save_user_address()){
                    Yii::$app->getSession()->setFlash('success', 'Your payment information has been updated.');
                }
            }            
        }
        
        $countries = Country::find()->orderBy('country_name')->all();
        return $this->render('billing_and_payments', [ 'model' => $model, 'countries' => $countries ]);
    }
    
    public function actionBillingHistory()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        $product_access = \Yii::$app->user->identity->product_access;
        if($is_payment_made == 'n' or $product_access == 3){
            return $this->goHome();
        }
        
        $user_id = \Yii::$app->user->identity->id;
        $main_user_id = \Yii::$app->user->identity->main_user_id;
        if($main_user_id > 0){
           $user_id = $main_user_id;
        }
        $model = BillingHistory::find()->where(['user_id' => $user_id])->orderBy('created_at', 'DESC')->all();
        return $this->render('billing_history', [ 'model' => $model ]);
    }
    
    public function actionUserManagement()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        $product_access = \Yii::$app->user->identity->product_access;
        if($is_payment_made == 'n' or $product_access != 1){
            return $this->goHome();
        }
        
        $user_id = \Yii::$app->user->identity->id;
        $model = UserManagement::find()->where(['main_user_id' => $user_id])->orderBy('id', 'DESC')->all(); 
        return $this->render('user_management', [ 'model' => $model ]);
    }
    
    public function actionCreateNewUser(){
        
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        $product_access = \Yii::$app->user->identity->product_access;
        if($is_payment_made == 'n' or $product_access != 1){
            return $this->goHome();
        }
        $model = new UserManagement();
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save_new_user()) {
                Yii::$app->getSession()->setFlash('success', 'New user has been added successfully.');
                return $this->redirect(array('/user/user-management'));
            }
        }
        return $this->render('create_new_user', [ 'model' => $model ]);
    }
    
    public function actionUpdateUser($id = NULL){
        
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        $product_access = \Yii::$app->user->identity->product_access;
        if($is_payment_made == 'n' or $product_access != 1 or $id == NULL){
            return $this->goHome();
        }
        
        $user_id = \Yii::$app->user->identity->id;
        $model = UserUpdateManagement::find()->where(['id' => $id, 'main_user_id' => $user_id])->one();
        if($model == NULL){
            return $this->redirect(array('/user/user-management'));
        } 
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->update_user($id)) {
                Yii::$app->getSession()->setFlash('success', 'User has been added successfully.');
                return $this->redirect(array('/user/user-management'));
            }
        }
        return $this->render('update_user', [ 'model' => $model ]);
    }
    
    function actionDownloadPdf($id = NULL){
        
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        $product_access = \Yii::$app->user->identity->product_access;
        if($is_payment_made == 'n' or $product_access == 3 or $id == NULL){
            return $this->goHome();
        }
        $payment_detail = BillingHistory::find()->where(['id' => $id])->one();
        if($payment_detail == NULL){
            return $this->goHome();
        }
        
        $user_address = UserAddress::find()->where(['user_id' => $payment_detail->user_id])->one();
        $order_information = \frontend\models\Order::find()->where(['user_id' => $payment_detail->user_id])->one();
        
        $content = $this->renderPartial('invoice_template', ['payment_detail' => $payment_detail, 'user_address' => $user_address, 'order_information' => $order_information]);
        //echo $content; die;
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '@import url(http://fonts.googleapis.com/css?family=Bree+Serif); .kv-heading-1{font-size:18px} body, h1, h2, h3, h4, h5, h6{ font-family: Bree Serif, serif; }', 
             // set mPDF properties on the fly
            'options' => ['title' => 'Invoice - JigsawTime'],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Invoice - JigsawTime'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        
        return $pdf->render(); 
        
    }


    public function actionRegister()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $model = new SignupForm();
        
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(array('/order/pricing'));
                }
            }
        }
        
        return $this->render('register', [
            'model' => $model,
        ]);
        
    }
    
    
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        $this->layout = "login";
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $is_payment_made = \Yii::$app->user->identity->is_payment_made;
            if($is_payment_made == 'n'){
                $post_data = Yii::$app->request->post();
                Yii::$app->getSession()->set('user_custom_password', $post_data['LoginForm']['password']);
                return $this->redirect(array('/order/pricing'));
            }else{
                return $this->redirect(array('/user'));
            }
            
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    
    public function actionRequestPasswordReset()
    {
        $this->layout = "login";
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }
    
    public function actionResetPassword($token)
    {
        $this->layout = "login";
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
    
}
