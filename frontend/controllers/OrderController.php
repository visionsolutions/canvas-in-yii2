<?php
namespace frontend\controllers;

use Yii;
use common\models\Country;
use frontend\models\UserAddress;
use frontend\models\Order;
use frontend\models\PaymentForm;
use frontend\models\SignupForm;
use frontend\models\BillingHistory;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class OrderController extends Controller
{
    public $layout = "user";
    
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'pricing', 'checkout', 'payment', 'confirm'],
                'rules' => [
                    [
                        'actions' => ['login', 'register'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['index', 'pricing', 'checkout', 'payment', 'confirm'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionIndex($id = NULL)
    {
        if($id != NULL){
            $package = \Yii::$app->params['packages'][$id - 1];
            $this->view->params['heading_tagline'] = "You're ordering a monthly subscription for 'Canvas'";
            return $this->render('index', array('package' => $package));
        }else{
            return $this->redirect(array('/order/pricing'));
        }
    }
    
    public function actionPricing()
    {
        $packages = \Yii::$app->params['packages'];
        $this->view->params['heading_tagline'] = 'Choose your Subscription Plan';
        return $this->render('pricing', array('packages' => $packages));
    }
    
    public function actionCheckout($id = NULL)
    {
        if($id != NULL){
            $user_id = \Yii::$app->user->identity->id;
            $model = UserAddress::find()->where(['user_id' => $user_id])->one();
            $order_model = Order::find()->where(['user_id' => $user_id])->one();
            if ($model == NULL){
                $model =  new UserAddress();
            }
            if ($order_model == NULL){
                $order_model =  new Order();
            }

            if ($model->load(Yii::$app->request->post()) and $order_model->load(Yii::$app->request->post())) {
                        
                if ($model->validate() and $order_model->validate()) {
                    $model->save_user_address();
                    if($latest_order = $order_model->save_user_order()){
                        Yii::$app->session['latest_order_id'] = $latest_order->id;
                        return $this->redirect(array('/order/payment', 'id' => $id));
                    }
                }
                     
            }
            $countries = Country::find()->orderBy('country_name')->all();
            $package = \Yii::$app->params['packages'][$id - 1];
            $this->view->params['heading_tagline'] = "You're ordering a monthly subscription for 'Canvas'";
            return $this->render('checkout', [
                'model' => $model,
                'order_model' => $order_model,
                'package' => $package,
                'countries' => $countries
            ]);
        }else{
            return $this->redirect(array('/order/pricing'));
        }
        
    }
    
    public function actionPayment($id = NULL)
    {
        if($id != NULL){
            $model = new PaymentForm();
            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate()){
                        
                        $post_data = Yii::$app->request->post();
                        $card_data['cart_type'] = $post_data['PaymentForm']['card_type'];
                        $card_data['cc_num'] = $post_data['PaymentForm']['card_number'];
                        $card_data['month'] = $post_data['PaymentForm']['expiration_month'];
                        $card_data['year'] = $post_data['PaymentForm']['expiration_year'];
                        $card_data['csc'] = $post_data['PaymentForm']['security_code'];                        
                        
                        $response = $this->paypal_charge($card_data);
                        if($response['ACK'] == 'Success'){
                            
                            $order_model = new Order();
                            $user_model = new SignupForm();
                            $billing_history_model = new BillingHistory();
                            
                            $order_model->update_user_order();
                            $user_model->update_user_status($response);
                            $billing_history_model->update_billing_history($id);
                            
                            $user['name'] = \Yii::$app->user->identity->name;
                            $user['username'] = \Yii::$app->user->identity->username;
                            $user['email_address'] = \Yii::$app->user->identity->email_address;
                            $user['custom_password'] = \Yii::$app->session->get('user_custom_password');
        
                            \Yii::$app->mailer->compose(['html' => 'accountInformation-html', 'text' => 'accountInformation-text'], ['user' => $user])
                                ->setFrom([\Yii::$app->params['supportEmail'] => \Yii::$app->params['application_name']])
                                ->setTo($user['email_address'])
                                ->setSubject('Account Information for ' . \Yii::$app->params['application_name'])
                                ->send();
                            
                            Yii::$app->session->remove('user_custom_password');
                            
                            return $this->redirect(array('/order/confirm'));
                            
                        }  else {
                            Yii::$app->getSession()->setFlash('error', $response['message']);
                        }
                    
                    
                }
            }
            
            $this->view->params['heading_tagline'] = "You're ordering a monthly subscription for 'Canvas'";
            return $this->render('payment', array('model' => $model));
        }else{
            return $this->redirect(array('/order/pricing'));
        }
        
    }
    
    public function actionConfirm()
    {
        $user_id = \Yii::$app->user->identity->id;
        $model = Order::find()->where(['user_id' => $user_id])->one();
        if ($model == NULL){
            return $this->redirect(array('/order/pricing'));
        }
        $this->view->params['heading_tagline'] = "Payment was successful, thank you.";
        return $this->render('confirm', array('model' => $model));
        
    }
    
    private function paypal_charge($post_data) {
        
        $return = array('ACK' => 'Failure', 'message' => '', 'paypal_response' => '');
        
        $user_id = \Yii::$app->user->identity->id;
        $model = UserAddress::find()->where(['user_id' => $user_id])->one();
        $order_model = Order::find()->where(['user_id' => $user_id])->one();
        $country = Country::find()->where(['country_abbrev' => $model->billing_country])->one();
        
        $data['AMT']                = $order_model->package_price;
        $data['ACCT']               = $post_data['cc_num'];
        $data['CREDITCARDTYPE']     = ucfirst($post_data['cart_type']);
        $data['EXPDATE']            = $post_data['month'] . $post_data['year'];
        $data['CVV2']               = $post_data['csc'];
        $data['FIRSTNAME']          = $model->billing_first_name;
        $data['LASTNAME']           = $model->billing_last_name;
        $data['STREET']             = $model->billing_address;
        $data['CITY']               = $model->billing_city;
        $data['STATE']              = '';
        $data['ZIP']                = '';
        $data['COUNTRYCODE']        = $model->billing_country;
        $data['COUNTRY']            = $country->country_name;
        $data['PROFILESTARTDATE']   = date('Y-m-d').'T00:00:00Z';
        $data['DESC']               = 'Canvas - '. $order_model->package_name;
        $data['BILLINGPERIOD']      = 'Month';
        $data['BILLINGFREQUENCY']   = '1';
        $data['TOTALBILLINGCYCLES'] = '0';
        $data['TAXAMOUNT']          = '0.00';
        $data['CURRENCYCODE']       = 'GBP';
        
        $response = \Yii::$app->mypaypal->createProfile($data);
        
        if (is_array($response) && $response['ACK'] == 'Success') {
            $return['ACK'] = 'Success';
            $return['paypal_response'] = $response;
        } else {
            $return['message'] = $response['L_LONGMESSAGE0'];
        }

        return $return;
    }

}
