<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * Site controller
 */
class FaqController extends Controller
{
    public $layout = "user";

    public function actionIndex()
    {
        $is_payment_made = \Yii::$app->user->identity->is_payment_made;
        if($is_payment_made == 'n'){
            return $this->goHome();
        }
        
        return $this->render('index');
    }

    
}
