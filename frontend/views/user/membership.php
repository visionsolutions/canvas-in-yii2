<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Menu;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Account Settings';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Content
============================================= -->

<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_one_third nobottommargin">


                <h3>Overview</h3>
                <?php
                $product_access = \Yii::$app->user->identity->product_access;
                $menuItems = array();
                $menuItems[] = ['label' => '<i class="icon-bullseye"></i> Membership', 'url' => ['/user/membership']];
                $menuItems[] = ['label' => '<i class="icon-unlock-alt"></i> Login Details', 'url' => ['/user/login-details']];
                $menuItems[] = ['label' => '<i class="icon-envelope"></i> Contact Information', 'url' => ['/user/contact-information']];
                if($product_access == 1){
                    $menuItems[] = ['label' => '<i class="icon-user"></i> User Management', 'url' => ['/user/user-management']];
                    $menuItems[] = ['label' => '<i class="icon-dollar"></i> Billing & Payments', 'url' => ['/user/billing-and-payments']];
                }
                if($product_access == 1 or $product_access == 2){
                    $menuItems[] = ['label' => '<i class="icon-comment"></i> Billing History', 'url' => ['/user/billing-history']];
                }
                echo Menu::widget([
                    'options' => ['class' => 'sidenav'],
                    'items' => $menuItems,
                    'activeCssClass' => 'ui-tabs-active',
                    'encodeLabels' => false,
                    'linkTemplate' => '<a href="{url}"><i></i>{label}<i class="icon-chevron-right"></i></a>',
                ]);
                
                $status_access['y'] = 'Active';
                $status_access['n'] = 'Inactive';
                ?>
                
                

            </div>

            <div class="col_two_third col_last nobottommargin">                    
                <div class="col_three_third col_first nobottommargin">
                    <?php $form = ActiveForm::begin(['id' => 'membership-form', 'options' => ['class' => 'nobottommargin']]); ?>
                    <h3>Membership</h3>
                    <?php
                    $success_message = Yii::$app->session->getFlash('success');
                    if ($success_message != "") {
                        ?>
                        <div class="alert alert-success" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Success:</span>
                            <?php echo $success_message; ?>
                        </div>
                    <?php } ?>
                    <?php echo $form->field($model, 'membership_status')->label('Membership status <span class="required">*</span>')->radioList($status_access); ?>
                    <?php echo Html::submitButton('Save & Update', ['class' => 'button button-3d button-black nomargin', 'id' => 'membership-form-submit', 'name' => 'membership-form-submit', 'value' => 'Save & Update']); ?>
                    <div class="clear"></div>
                    <div class="divider divider-center">
                        <i class="icon-cloud"></i>
                    </div>
                    <h3>Cancel</h3>
                    <a href="javascript:void(0);" onclick="cancel_my_account();">Cancel my account</a>
                    <?php echo Html::a('Cancel my account', ['user/cancel-my-account'], ['class' => 'hide', 'id' => 'cancel_my_account_link']); ?>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>
            <script>
                    function cancel_my_account(){
                        var r = confirm("Are you sure, You want to cancel your account?");
                        if (r == true) {
                            document.getElementById('cancel_my_account_link').click();
                        }
                    }
            </script>
            </section>
            <!-- #content end -->