<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Reset your password';
?>
<div class="panel-body" style="padding: 40px;">
    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
    <h3><?= Html::encode($this->title) ?></h3>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <div class="col_full nobottommargin">
        <?php echo Html::submitButton('Save', ['class' => 'button button-3d button-black nomargin', 'id' => 'reset-password-form-submit', 'name' => 'reset-password-form-submit', 'value' => 'Save']); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>