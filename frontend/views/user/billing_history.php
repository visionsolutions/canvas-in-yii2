<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Menu;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Account Settings';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Content
============================================= -->

<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_one_third nobottommargin">


                <h3>Overview</h3>
                <?php
                $product_access = \Yii::$app->user->identity->product_access;
                $menuItems = array();
                $menuItems[] = ['label' => '<i class="icon-bullseye"></i> Membership', 'url' => ['/user/membership']];
                $menuItems[] = ['label' => '<i class="icon-unlock-alt"></i> Login Details', 'url' => ['/user/login-details']];
                $menuItems[] = ['label' => '<i class="icon-envelope"></i> Contact Information', 'url' => ['/user/contact-information']];
                if($product_access == 1){
                    $menuItems[] = ['label' => '<i class="icon-user"></i> User Management', 'url' => ['/user/user-management']];
                    $menuItems[] = ['label' => '<i class="icon-dollar"></i> Billing & Payments', 'url' => ['/user/billing-and-payments']];
                }
                if($product_access == 1 or $product_access == 2){
                    $menuItems[] = ['label' => '<i class="icon-comment"></i> Billing History', 'url' => ['/user/billing-history']];
                }
                echo Menu::widget([
                    'options' => ['class' => 'sidenav'],
                    'items' => $menuItems,
                    'activeCssClass' => 'ui-tabs-active',
                    'encodeLabels' => false,
                    'linkTemplate' => '<a href="{url}"><i></i>{label}<i class="icon-chevron-right"></i></a>',
                ]);
                ?>




            </div>

            <div class="col_two_third col_last nobottommargin">                    
                <div class="col_three_third col_first nobottommargin">
                    <h3>Billing history</h3>

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Amount</th>
                                    <th>PDF</th>
                                    <th>Paid</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(count($model) > 0) { $counter = 1; ?>
                                <?php foreach($model as $history) { ?>
                                <tr>
                                    <td><?php echo $counter; ?></td>
                                    <td><?php echo date('M d, y', strtotime($history->created_at)); ?></td>
                                    <td>Close</td>
                                    <td>$<?php echo $history->amount; ?></td>
                                    <td>
                                        <?php echo Html::a('Download pdf', ['user/download-pdf', 'id' => $history->id]); ?>
                                    </td>
                                    <td>Yes</td>
                                </tr>
                                <?php $counter++; } } else { ?>
                                <tr>
                                    <td colspan="4" style="text-align: center"> No Record Found </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- #content end -->