<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Request password reset';
?>


<div class="panel-body" style="padding: 40px;">
    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
    
    <h3><?php echo Html::encode($this->title); ?></h3>
    
    <?php echo $form->field($model, 'email_address'); ?>
    <div class="col_full nobottommargin">
        <?= Html::submitButton('Send', ['class' => 'button button-3d button-black nomargin']) ?>
        <?php echo Html::a('Login', ['user/login'], ['class' => 'fright']); ?>
    </div>
    <?php ActiveForm::end(); ?>

    <div class="line line-sm"></div>
    <b><?php echo Yii::$app->session->getFlash('success'); ?></b>
    <b><?php echo Yii::$app->session->getFlash('error'); ?></b>
    <!--  facebook login was here -->
</div>



