<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Stats';
?>

<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-parallax page-title-dark page-title-video">

            <div class="video-wrap">
                <video poster="/canvas/images/videos/stats.jpg" preload="auto" loop autoplay muted>
                    <source src='/canvas/images/videos/expl/ore.mp4' type='video/mp4' />
                    <source src='/canvas/images/videos/expl/ore.webm' type='video/webm' />
                </video>
                <div class="video-overlay"></div>
            </div>

            <div class="container clearfix">
                <h1>STATS</h1>
                <span>Important statistics about your account</span>
                
            </div>
        </section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">


            <!-- STATS CANVAS -->
            <div class="col_one_third nobottommargin center">
                <div class="rounded-skill nobottommargin" data-color="#19B999" data-size="200" data-percent="1" data-width="4" data-animate="6500">

                    <div class="counter counter-inherit"><span data-from="1" data-to="1" data-refresh-interval="50" data-speed="6000"></span>%</div>

                </div>
            </div>

            <div class="col_one_third nobottommargin center">
                <div class="rounded-skill nobottommargin" data-color="#D9534F" data-size="200" data-percent="85" data-width="4" data-animate="6500">
                    <div class="counter counter-inherit"><span data-from="1" data-to="85" data-refresh-interval="50" data-speed="6000"></span>%</div>

                </div>
            </div>

            <div class="col_one_third nobottommargin center col_last">
                <div class="rounded-skill nobottommargin" data-color="#3F729B" data-size="200" data-percent="2" data-width="3" data-animate="6500">
                    <div class="counter counter-inherit"><span data-from="1" data-to="2" data-refresh-interval="50" data-speed="6000"></span>%</div>

                </div>
            </div>




            <div class="col_one_third text-center">
                <h3>Detection ratio</h3>
                Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula.
            </div>

            <div class="col_one_third text-center">
                <h3>Scanning Speed</h3>
                Your scanning speed is below 80%. This is ok. 
            </div>

            <div class="col_one_third col_last text-center">
                <h3>Light conditions</h3>
                Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula.
            </div>




            <div class="clear"></div>


            <!-- STATS CANVAS -->
            <div class="col_one_third nobottommargin center">
                <div class="rounded-skill nobottommargin" data-color="#19B999" data-size="200" data-percent="55" data-width="4" data-animate="6500">

                    <div class="counter counter-inherit"><span data-from="1" data-to="55" data-refresh-interval="50" data-speed="6000"></span>%</div>

                </div>
            </div>

            <div class="col_one_third nobottommargin center">
                <div class="rounded-skill nobottommargin" data-color="#D9534F" data-size="200" data-percent="85" data-width="4" data-animate="6500">
                    <div class="counter counter-inherit"><span data-from="1" data-to="85" data-refresh-interval="50" data-speed="6000"></span>%</div>

                </div>
            </div>

            <div class="col_one_third nobottommargin center col_last">
                <div class="rounded-skill nobottommargin" data-color="#3F729B" data-size="200" data-percent="2" data-width="3" data-animate="6500">
                    <div class="counter counter-inherit"><span data-from="1" data-to="2" data-refresh-interval="50" data-speed="6000"></span>%</div>

                </div>
            </div>




            <div class="col_one_third text-center">
                <h3>Usage</h3>
                Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula.
            </div>

            <div class="col_one_third text-center">
                <h3>Captured errors</h3>
                The amount of errors the system catches, in percent %.
            </div>

            <div class="col_one_third col_last text-center">
                <h3>Improvents</h3>
                Donec sed odio dui. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula.
            </div>




        </div>

    </div>

</section>

<!-- #content end -->
<footer>
            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                    <div class="col_half">
                        Copyrights &copy; <?php echo date('Y'); ?> All Rights Reserved by Vision Solutions Ltd.<br>

                    </div>

                    <div class="col_half col_last tright">
                        <div class="fright clearfix">

                        </div>

                        <div class="clear"></div>


                    </div>

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->