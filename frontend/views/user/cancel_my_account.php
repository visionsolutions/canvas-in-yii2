<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Menu;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Account Settings';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Content
============================================= -->

<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col_two_third col_last nobottommargin">                    
                <div class="col_three_third col_first nobottommargin">
                    <p>Your account has been cancel successfully.</p>
                </div>
            </div>
        </div>
    </div>
</section>
            <!-- #content end -->