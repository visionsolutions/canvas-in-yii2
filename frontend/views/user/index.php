<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Dashboard';
?>

<!-- Content
============================================= -->
<section id="slider" class="slider-parallax full-screen dark error404-wrap">

    <div class="container vertical-middle center clearfix">

        <div class="error404" id="error404_id">Camera</div>


        <h3 id="quality_checker">Start the quality checker by clicking the "Start" button.</h3>
        <form action="#" method="get" role="form" class="divcenter nobottommargin">
            <div class="input-group input-group-lg">
                <span class="input-group-btn">
                    <button id="capture-button" class="button button-3d button-xlarge button-red" type="button"><i class="icon-play-circle"></i>&nbsp;<b>START</b></button>
                </span>
                <!--<input type="text" class="fo/rm-control" >-->
                <span class="input-group-btn">
                    <button id="stop-button" class="button button-3d button-xlarge" type="button"><i class="icon-off"></i>&nbsp; <b>STOP</b></button>
                </span>
            </div>
        </form>

    </div>

    <div class="video-wrap">
        <video id="basic-stream" class="videostream" preload="auto" loop autoplay muted ></video>
<!--        <video poster="/canvas/images/videos/explore-poster.jpg" preload="auto" loop autoplay muted>
            <source src='/canvas/images/videos/exp/lore.mp4' type='video/mp4' />
            <source src='/canvas/images/videos/exp/lore.webm' type='video/webm' />
        </video>-->
        <div class="video-overlay" style="background-color: rgba(0,0,0,0.3);"></div>
    </div>

</section>
<script>
    function errorCallback(e) {
        if (e.code == 1) {
          alert('User denied access to their camera');
        } else {
          alert('getUserMedia() not supported in your browser.');
        }
    }
    
    (function() {
        var video = document.querySelector('#basic-stream');
        var button = document.querySelector('#capture-button');
        var localMediaStream = null;

        button.addEventListener('click', function(e) {
          document.getElementById("quality_checker").style.visibility = "hidden";
          document.getElementById("error404_id").style.visibility = "hidden";
          
          if (navigator.getUserMedia) {
            navigator.getUserMedia('video', function(stream) {
              video.src = stream;
              video.controls = true;
              localMediaStream = stream;
            }, errorCallback);
          } else if (navigator.webkitGetUserMedia) {
            navigator.webkitGetUserMedia({video: true}, function(stream) {
              video.src = window.URL.createObjectURL(stream);
              video.controls = true;
              localMediaStream = stream;
            }, errorCallback);
          } else {
            errorCallback({target: video});
          }
        }, false);

        document.querySelector('#stop-button').addEventListener('click', function(e) {
          document.getElementById("quality_checker").style.visibility = "";  
          document.getElementById("error404_id").style.visibility = "";
          video.pause();
          localMediaStream.stop(); // Doesn't do anything in Chrome.
        }, false);
        })();
        
    window.isCompatible = function() {
          return !!(navigator.getUserMedia || navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia || navigator.msGetUserMedia);
    };
     if (isCompatible() === false) {
      document.getElementById('notcompatible').className = '';
    }
    function _prettyPrint() {
      if (typeof customPrettyPrintLanguage != 'undefined') {
        customPrettyPrintLanguage();
      }
      prettyPrint();
    }
</script>
<!-- #content end -->