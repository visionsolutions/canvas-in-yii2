<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Menu;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Account Settings';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Content
============================================= -->

<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_one_third nobottommargin">


                <h3>Overview</h3>
                <?php
                $product_access = \Yii::$app->user->identity->product_access;
                $menuItems = array();
                $menuItems[] = ['label' => '<i class="icon-bullseye"></i> Membership', 'url' => ['/user/membership']];
                $menuItems[] = ['label' => '<i class="icon-unlock-alt"></i> Login Details', 'url' => ['/user/login-details']];
                $menuItems[] = ['label' => '<i class="icon-envelope"></i> Contact Information', 'url' => ['/user/contact-information']];
                if($product_access == 1){
                    $menuItems[] = ['label' => '<i class="icon-user"></i> User Management', 'url' => ['/user/user-management']];
                    $menuItems[] = ['label' => '<i class="icon-dollar"></i> Billing & Payments', 'url' => ['/user/billing-and-payments']];
                }
                if($product_access == 1 or $product_access == 2){
                    $menuItems[] = ['label' => '<i class="icon-comment"></i> Billing History', 'url' => ['/user/billing-history']];
                }
                echo Menu::widget([
                    'options' => ['class' => 'sidenav'],
                    'items' => $menuItems,
                    'activeCssClass' => 'ui-tabs-active',
                    'encodeLabels' => false,
                    'linkTemplate' => '<a href="{url}"><i></i>{label}<i class="icon-chevron-right"></i></a>',
                ]);
                ?>




            </div>

            <div class="col_two_third col_last nobottommargin">                    
                <div class="col_three_third col_first nobottommargin">
                    <h3>Login details</h3>
                    <?php
                    $success_message = Yii::$app->session->getFlash('success');
                    if ($success_message != "") {
                        ?>
                        <div class="alert alert-success" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Success:</span>
                            <?php echo $success_message; ?>
                        </div>
                    <?php } ?>
                    <?php $form = ActiveForm::begin(['id' => 'register-form', 'options' => ['class' => 'nobottommargin']]); ?>
                    <?php echo $form->field($model, 'username', ['template' => '<div class="col_half">{label}{input}{error}</div>'])->label('Username <span class="required">*</span>'); ?>
                    <?php echo $form->field($model, 'password', ['template' => '<div class="col_half col_last">{label}{input}{error}</div>'])->passwordInput(); ?>
                    <?php echo $form->field($model, 'email_address', ['template' => '<div class="col_half">{label}{input}{error}</div>'])->label('Email Address <span class="required">*</span>'); ?>
                    <?php echo $form->field($model, 'security_answer', ['template' => '<div class="col_half col_last">{label}{input}{error}</div>'])->label("What's your favourite color? (security) <span class='required'>*</span>"); ?>    
                    <div class="clear"></div>
                    <?php echo Html::submitButton('Save & Update', ['class' => 'button button-3d button-black nomargin', 'id' => 'register-form-submit', 'name' => 'register-form-submit', 'value' => 'Save & Update']); ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- #content end -->