<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Login';
?>


<div class="panel-body" style="padding: 40px;">
    <?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['class' => 'nobottommargin']]); ?>
    <h3>Login to your Account</h3>
    <?php echo $form->field($model, 'username'); ?>
    <?php echo $form->field($model, 'password')->passwordInput(); ?>
    <div class="col_full nobottommargin">
        <?php echo Html::submitButton('Login', ['class' => 'button button-3d button-black nomargin', 'id' => 'login-form-submit', 'name' => 'login-form-submit', 'value' => 'Login']); ?>
        <?php echo Html::a('Forgot Password?', ['user/request-password-reset'], ['class' => 'fright']); ?>
    </div>
    <?php ActiveForm::end(); ?>

    <div class="line line-sm"></div>
    Don't have an Account? <?php echo Html::a('Register Now', ['user/register']); ?>
    <!--  facebook login was here -->
</div>

