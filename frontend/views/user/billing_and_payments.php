<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\widgets\Menu;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Account Settings';
$this->params['breadcrumbs'][] = $this->title;

$countries_list = array();
foreach ($countries as $country){
   $countries_list[$country->country_abbrev] = $country->country_name;
}

?>

<!-- Content
============================================= -->

<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_one_third nobottommargin">


                <h3>Overview</h3>
                <?php
                $product_access = \Yii::$app->user->identity->product_access;
                $menuItems = array();
                $menuItems[] = ['label' => '<i class="icon-bullseye"></i> Membership', 'url' => ['/user/membership']];
                $menuItems[] = ['label' => '<i class="icon-unlock-alt"></i> Login Details', 'url' => ['/user/login-details']];
                $menuItems[] = ['label' => '<i class="icon-envelope"></i> Contact Information', 'url' => ['/user/contact-information']];
                if($product_access == 1){
                    $menuItems[] = ['label' => '<i class="icon-user"></i> User Management', 'url' => ['/user/user-management']];
                    $menuItems[] = ['label' => '<i class="icon-dollar"></i> Billing & Payments', 'url' => ['/user/billing-and-payments']];
                }
                if($product_access == 1 or $product_access == 2){
                    $menuItems[] = ['label' => '<i class="icon-comment"></i> Billing History', 'url' => ['/user/billing-history']];
                }
                echo Menu::widget([
                    'options' => ['class' => 'sidenav'],
                    'items' => $menuItems,
                    'activeCssClass' => 'ui-tabs-active',
                    'encodeLabels' => false,
                    'linkTemplate' => '<a href="{url}"><i></i>{label}<i class="icon-chevron-right"></i></a>',
                ]);
                ?>




            </div>

            <div class="col_two_third col_last nobottommargin">                    
                <div class="col_three_third col_first nobottommargin">
                    <h3>Payment Information</h3>
                    <?php
                    $success_message = Yii::$app->session->getFlash('success');
                    if ($success_message != "") {
                        ?>
                        <div class="alert alert-success" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Success:</span>
                            <?php echo $success_message; ?>
                        </div>
                    <?php } ?>
                    <div class="col_full">
                        <?php
                            $is_payment_made = \Yii::$app->user->identity->is_payment_made;
                            $membership_status = \Yii::$app->user->identity->membership_status;
                            if($is_payment_made == 'y' and $membership_status == 'y'){
                        ?>
                                Credit card is active: <span class="label label-success">Yes</span>
                            <?php } else if($membership_status == 'n') { ?>   
                                Your account is : <span class="label label-danger">Suspend</span>
                            <?php }else { ?>
                                Credit card is active: <span class="label label-danger">No</span>
                            <?php } ?>
                    </div>
                    <?php $form = ActiveForm::begin(['id' => 'billing-form', 'options' => ['class' => 'nobottommargin']]); ?>
                    <div id="ptab2">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <h3>Billing Address</h3>
                                <p class="note" style="margin-bottom: 0px;">Fields with <span class="required">*</span> are required.</p>
                                <?php echo $form->field($model, 'billing_first_name')->label('First Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_last_name')->label('Last Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_company_name')->label('Company Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_vat_number')->label('VAT Number <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_address')->label('Address <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_city')->label('City <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_country')->label('Country <span class="required">*</span>')->dropDownList($countries_list, ['prompt'=>'Select Country'] ); ?>
                                <?php echo Html::activeHiddenInput($model, 'billing_email_address'); ?>
                                <?php echo Html::activeHiddenInput($model, 'billing_phone'); ?>
                            </div>
                            <div class="col-md-6">
                                <h3 class="">Shipping Address</h3>
                                <p class="note" style="margin-bottom: 0px;">Fields with <span class="required">*</span> are required.</p>
                                <?php echo $form->field($model, 'shipping_first_name')->label('First Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_last_name')->label('Last Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_company_name')->label('Company Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_vat_number')->label('VAT Number <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_address')->label('Address <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_city')->label('City <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_country')->label('Country <span class="required">*</span>')->dropDownList($countries_list, ['prompt'=>'Select Country'] ); ?>    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="text-align: center;"><input type="checkbox" name="billing_same_shipping" id="billing_same_shipping" value="y" onclick="billing_same_shipping_fun();" > Shipping Address is same as Billing Address.</div>
                    <?php echo Html::submitButton('Save & Update', ['class' => 'button button-3d button-black nomargin', 'id' => 'billing-form-submit', 'name' => 'billing-form-submit', 'value' => 'Save & Update']); ?>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function billing_same_shipping_fun(){
        if($('#billing_same_shipping').prop('checked')) {    
            $('#useraddress-shipping_first_name').val($('#useraddress-billing_first_name').val());
            $('#useraddress-shipping_last_name').val($('#useraddress-billing_last_name').val());
            $('#useraddress-shipping_company_name').val($('#useraddress-billing_company_name').val());
            $('#useraddress-shipping_vat_number').val($('#useraddress-billing_vat_number').val());
            $('#useraddress-shipping_address').val($('#useraddress-billing_address').val());
            $('#useraddress-shipping_city').val($('#useraddress-billing_city').val());
            $('#useraddress-shipping_country').val($('#useraddress-billing_country').val());
        }
    }
</script>
<!-- #content end -->