<?php

use yii\helpers\Html;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <?= Html::csrfMetaTags() ?>
    </head>
    <body>
        <?php $this->beginBody() ?> 

        <?php
        $invoice_number = $payment_detail->id;
        if ($invoice_number < 9) {
            $invoice_number = '00' . $invoice_number;
        }
        if ($invoice_number > 9 and $invoice_number < 99) {
            $invoice_number = '0' . $invoice_number;
        }
        ?>
        <div class="container">
            <div class="row" >
                <div class="col-xs-5" style="width: 50%; float: left;" >
                    <h1>
                        <img src="http://jigsawtime.co.uk/canvas/images/jigsawtime_logo.png">
                    </h1>
                </div>
                <div class="col-xs-5 text-right" style="width: 40%; float: left;  padding-right: 5x; margin-right: 0px;" >
                    <h3>INVOICE</h3>
                    <h3><small>Invoice #<?php echo $invoice_number; ?></small></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-5" style="width: 45%;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 style="font-size: 15px;">From: Vision Technology Consultants Ltd.</h4>
                        </div>
                        <div class="panel-body">
                            <p>
                                6th September 160,<br /> Plovdiv 4002, <br />Bulgaria<br>



                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-5  text-right" style="width: 45%;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 style="font-size: 15px;">To : |<?php echo $user_address->billing_first_name; ?> <?php echo $user_address->billing_last_name; ?>|</h4>
                        </div>
                        <div class="panel-body">
                            <p>
<?php echo $user_address->billing_address; ?>,<br /> <?php echo $user_address->billing_city; ?>, <br /><?php echo $user_address->billing_country; ?><br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- / end client details section -->
            <table class="table table-bordered" style="width: 99%;">
                <thead>
                    <tr>
                        <th>
                <h4 style="font-size: 14px;">Service</h4>
                </th>
                <th>
                <h4 style="font-size: 14px;">Description</h4>
                </th>

                <th>
                <h4 style="font-size: 14px;">Price</h4>
                </th>
                <th>
                <h4 style="font-size: 14px;">Sub Total</h4>
                </th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="font-size: 13px;font-weight: bold"><?php echo $order_information->package_name; ?></td>
                        <td style="font-size: 13px;font-weight: bold">Fabric Quality Checking Software (SaaS)</td>
                        <td class="text-right" style="font-size: 13px;font-weight: bold">&euro; <?php echo number_format($payment_detail->amount, 2); ?></td>
                        <td class="text-right" style="font-size: 13px;font-weight: bold">&euro; <?php echo number_format($payment_detail->amount, 2); ?></td>

                    </tr>
                </tbody>
            </table>
            <div class="row text-right" >
                <div class="col-xs-9">
                    <p>
                        <strong style="font-size: 13px;font-weight: bold">
                            Sub Total : <br>
                            VAT TAX : <br>
                            Total : <br>
                        </strong>
                    </p>
                </div>
                <div class="col-xs-2" style="margin: 0px; padding: 0px;">
                    <strong style="font-size: 13px;">
                        &euro; <?php echo number_format($payment_detail->amount, 2); ?><br>
                        N/A <br>
                        &euro; <?php echo number_format($payment_detail->amount, 2); ?> <br>
                    </strong>
                </div>
            </div>
            <div class="row">

                <div class="col-xs-5" style="width: 45%;">
                    <div class="span7">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 style="font-size: 15px;">Contact Details</h4>
                            </div>
                            <div class="panel-body">
                                <p style="font-size: 13px;font-weight: bold;">
                                    Email  : support@jigsawtime.co.uk <br><br>
                                    Phone  : +359 (0)87 713-1474 <br> <br>


                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php $this->endBody() ?> 
    </body>
</html>
<?php $this->endPage() ?>