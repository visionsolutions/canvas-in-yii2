<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Register New Account';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Content
============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <div class="col_full col_last nobottommargin">
                <h3>Don't have an Account? Register Now.</h3>
                <?php $form = ActiveForm::begin(['id' => 'register-form', 'options' => ['class' => 'nobottommargin']]); ?>
                <br>
                <?php echo $form->field($model, 'name'); ?>
                <?php echo $form->field($model, 'email_address'); ?>
                <?php echo $form->field($model, 'phone'); ?>
                <?php echo $form->field($model, 'company_name'); ?>
                <div class="col_full nobottommargin">
                    <?php echo Html::submitButton('Register Now', ['class' => 'button button-3d button-black nomargin', 'id' => 'register-form-submit', 'name' => 'register-form-submit', 'value' => 'Register Now']); ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>
<!-- #content end -->