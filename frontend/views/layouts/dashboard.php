<?php

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
        <?php $this->head() ?>
        <!--[if lt IE 9]>
        <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    </head>
    <body class="stretched">
        <?php $this->beginBody() ?>

        <!-- Document Wrapper
    ============================================= -->
        <div id="wrapper" class="clearfix">


            <!-- Header
            ============================================= -->
            <header id="header" class="full-header">

                <div id="header-wrap">

                    <div class="container clearfix">

                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                        <!-- Logo
                        ============================================= -->
                        <div id="logo">
                            <a href="<?php echo Yii::$app->homeUrl; ?>" class="standard-logo" data-dark-logo="/canvas/images/logo-dark.png"><img src="/canvas/images/logo.png" alt="Canvas Logo"></a>
                            <a href="<?php echo Yii::$app->homeUrl; ?>" class="retina-logo" data-dark-logo="/canvas/images/logo-dark@2x.png"><img src="/canvas/images/logo@2x.png" alt="Canvas Logo"></a>
                        </div><!-- #logo end -->

                        <!-- Primary Navigation
                        ============================================= -->
                        <nav id="primary-menu">
                            <?php
                            
                            if (Yii::$app->user->isGuest) {
                                $menuItems[] = ['label' => 'Home', 'url' => ['/site/index']];
                                $menuItems[] = ['label' => 'Register Now', 'url' => ['/user/register']];
                                $menuItems[] = ['label' => 'Login', 'url' => ['/user/login']];
                            } else {
                                $menuItems[] = ['label' => '<i class="icon-dashboard"></i> Dashboard', 'url' => ['/user']];
                                $menuItems[] = ['label' => '<i class="icon-bar-chart"></i> Stats', 'url' => ['/user/stats']];
                                $menuItems[] = ['label' => '<i class="icon-book"></i> FAQ', 'url' => ['/faq']];
                                $menuItems[] = ['label' => '<i class="icon-cog"></i> Settings', 'url' => ['/user/settings']];
                                $menuItems[] = ['label' => '<i class="icon-external-link"></i> Logout', 'url' => ['/user/logout']];
                            }
                            echo Menu::widget([
                                'options' => ['class' => 'one-page-menu'],
                                'items' => $menuItems,
                                'activeCssClass' => 'tab-current',
                                'encodeLabels' => false,
                                'linkTemplate' => '<a href="{url}"><div>{label}</div></a>',
                            ]);
                            ?>
                            
                            <?php if (Yii::$app->user->isGuest) {}else{ ?>
                            <div id="top-cart">
                                <a id="top-cart-trigger" href="#"><i class="icon-comments"></i><span>2</span></a>
                            </div>
                            <?php } ?>
                            
                        </nav><!-- #primary-menu end -->


                        <!-- Top Search
                        ============================================= -->
                        <!-- #top-search end -->

                        <!-- #primary-menu end -->

                    </div>

                </div>

            </header>

            <!-- #header end -->



            <?php echo $content; ?>


        </div><!-- #wrapper end -->

        <!-- Go To Top
        ============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>