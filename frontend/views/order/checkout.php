<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = 'Register New Account';
$this->params['breadcrumbs'][] = 'Pricing';
$this->params['breadcrumbs'][] = $this->title;

$countries_list = array();
foreach ($countries as $country){
   $countries_list[$country->country_abbrev] = $country->country_name;
}


?>

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div id="processTabs">
                <ul class="process-steps bottommargin clearfix">
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">1</a>
                        <h5>Review Cart</h5>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">2</a>
                        <h5>Enter Shipping Info</h5>


                    </li>
                    <li>
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">3</a>
                        <h5>Complete Payment</h5>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">4</a>
                        <h5>Order Complete</h5>
                    </li>
                </ul>
                <div>
                    <?php $form = ActiveForm::begin(['id' => 'billing-form', 'options' => ['class' => 'nobottommargin']]); ?>
                    <div id="ptab2">
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <h3>Billing Address</h3>
                                <p>Your billing address is where you receive your bank statement or invoice for the credit card. The billing address should match your company adress. It may vary in different countries.</p>
                                <p class="note">Fields with <span class="required">*</span> are required.</p>
                                <?php echo $form->field($model, 'billing_first_name')->label('First Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_last_name')->label('Last Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_company_name')->label('Company Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_vat_number')->label('VAT Number <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_address')->label('Address <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_city')->label('City <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_country')->label('Country <span class="required">*</span>')->dropDownList($countries_list, ['prompt'=>'Select Country'] ); ?>
                                <?php echo $form->field($model, 'billing_email_address')->label('Email Address <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'billing_phone')->label('Phone <span class="required">*</span>'); ?>
                                <div class="form-group "><input type="checkbox" name="billing_same_shipping" id="billing_same_shipping" value="y" onclick="billing_same_shipping_fun();" > Shipping Address is same as Billing Address.</div>
                            </div>
                            <div class="col-md-6">
                                <h3 class="">Shipping Address</h3>
                                <p>The shipping address is where you want your items delivered. The Digital Video Camera and Mechanical arm will be shipped to your chosen adress. This can be the same as your billing address, or it can be different.</p>
                                <p class="note">Fields with <span class="required">*</span> are required.</p>
                                <?php echo $form->field($model, 'shipping_first_name')->label('First Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_last_name')->label('Last Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_company_name')->label('Company Name <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_vat_number')->label('VAT Number <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_address')->label('Address <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_city')->label('City <span class="required">*</span>'); ?>
                                <?php echo $form->field($model, 'shipping_country')->label('Country <span class="required">*</span>')->dropDownList($countries_list, ['prompt'=>'Select Country'] ); ?>    
                                <?php echo $form->field($order_model, 'note')->textarea(array('rows' => 6, 'cols' => 30))->label('Note <span class="required">*</span>'); ?>
                            </div>


                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="clear bottommargin"></div>
                    <div class="col-md-6">
                        <div class="table-responsive clearfix">
                            <h4>Your Order</h4>

                            <table class="table cart">
                                <thead>
                                    <tr>
                                        <th class="cart-product-name">Product</th>
                                        <th class="cart-product-subtotal">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="cart_item">
                                        <td class="cart-product-name">
                                            <?php echo $package['package_name']; ?>
                                        </td>



                                        <td class="cart-product-subtotal">
                                            <span class="amount">€ <?php echo $package['package_price']; ?> / Month</span>
                                        </td>
                                    </tr>
                                    
                                    
                                </tbody>

                            </table>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="table-responsive">
                            <h4>Cart Totals</h4>

                            <table class="table cart">
                                <tbody>
                                    <tr class="cart_item">
                                        <td class="notopborder cart-product-name">
                                            <strong>Monthly subscription price</strong>
                                        </td>

                                        <td class="notopborder cart-product-name">
                                            <span class="amount">€ <?php echo $package['package_price']; ?></span>
                                        </td>
                                    </tr>
                                    
                                    <tr class="cart_item">
                                        <td class="cart-product-name">
                                            <strong>Total month #1</strong>
                                        </td>

                                        <td class="cart-product-name">
                                            <span class="amount color lead"><strong>€ <?php echo $package['package_price']; ?></strong></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="accordion clearfix">
                            <h4>Choose Payment method</h4>
                            <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Paypal</div>
                            <div class="acc_content clearfix"><input name="payment_method" id="payment_method_paypal" type="radio" value="paypal" checked=""> <label for="payment_method_paypal" style="font-weight: normal;">Paypal Payments (Visa, Mastercard etc.)</label></div>

                            <div class="acctitle"><i class="acc-closed icon-ok-circle"></i><i class="acc-open icon-remove-circle"></i>Google Wallet</div>
                            <div class="acc_content clearfix"><input name="payment_method" id="payment_method_google" type="radio" value="google"> <label for="payment_method_google" style="font-weight: normal;">Google Wallet (Visa, Mastercard etc.)</label></div>


                        </div>
                        <?php echo Html::submitButton('Place Order', ['class' => 'button button-3d fright', 'id' => 'billing-form-submit', 'name' => 'billing-form-submit', 'value' => 'Place Order']); ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>

                </section>
<script>
    function billing_same_shipping_fun(){
        if($('#billing_same_shipping').prop('checked')) {    
            $('#useraddress-shipping_first_name').val($('#useraddress-billing_first_name').val());
            $('#useraddress-shipping_last_name').val($('#useraddress-billing_last_name').val());
            $('#useraddress-shipping_company_name').val($('#useraddress-billing_company_name').val());
            $('#useraddress-shipping_vat_number').val($('#useraddress-billing_vat_number').val());
            $('#useraddress-shipping_address').val($('#useraddress-billing_address').val());
            $('#useraddress-shipping_city').val($('#useraddress-billing_city').val());
            $('#useraddress-shipping_country').val($('#useraddress-billing_country').val());
        }
    }
</script>
                <!-- #content end -->