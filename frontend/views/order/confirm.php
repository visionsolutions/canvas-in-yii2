<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Order Confirmation';
$this->params['breadcrumbs'][] = 'Register New Account';
$this->params['breadcrumbs'][] = 'Pricing';
$this->params['breadcrumbs'][] = 'Checkout';
?>

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div id="processTabs">
                <ul class="process-steps bottommargin clearfix">
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">1</a>
                        <h5>Review Cart</h5>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">2</a>
                        <h5>Enter Shipping Info</h5>


                    </li>
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">3</a>
                        <h5>Complete Payment</h5>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">4</a>
                        <h5>Order Complete</h5>
                    </li>
                </ul>
                <div>

                    <div class="container clearfix">



                        <div class="row clearfix">
                            <div class="col-md-6">

                                

                                <h4>Here are your order details:</h4>

                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>

                                                <th>Item</th>
                                                <th>Status</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>

                                                <td>Subscription:</td>
                                                <td><?php echo $model['package_name']; ?></td>

                                            </tr>
                                            <tr>

                                                <td>Price:</td>
                                                <td>€ <?php echo $model['package_price']; ?></td>

                                            </tr>
                                            <tr>

                                                <td>Payment:</td>
                                                <td>Confirmed</td>

                                            </tr>
                                            <tr>

                                                <td>Total:</td>
                                                <td>€ <?php echo $model['package_price']; ?></td>

                                            </tr>


                                            <tr>

                                                <td>Login details:</td>
                                                <td>Check your email account</td>

                                            </tr>
                                            

                                        </tbody>
                                    </table>
                                </div><!-- /.table-responsive -->

                                <br />

                                <strong>Questions?</strong><br />
                                If you have any questions or concerns regarding your order
                                please visit our website by clicking the Support link 
                                at the bottom of the website.

                                We hope you enjoy using the Quality checker!
                                <br >
                                --<br />
                                Fabric Quality Checking Team



                            </div>




                            <div class="clear bottommargin"></div>

                        </div>

                    </div>

                    <div class="clear"></div>



                </div>

                </section>

                <!-- #content end -->