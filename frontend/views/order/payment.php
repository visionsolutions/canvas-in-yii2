<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Payment Information';
$this->params['breadcrumbs'][] = 'Register New Account';
$this->params['breadcrumbs'][] = 'Pricing';
$this->params['breadcrumbs'][] = 'Checkout';
?>

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div id="processTabs">
                <ul class="process-steps bottommargin clearfix">
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">1</a>
                        <h5>Review Cart</h5>
                    </li>
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">2</a>
                        <h5>Enter Shipping Info</h5>


                    </li>
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">3</a>
                        <h5>Complete Payment</h5>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">4</a>
                        <h5>Order Complete</h5>
                    </li>
                </ul>
                <div>

                    <div class="row clearfix">
                        <div class="col-md-6">
                            <h3>Card info</h3>
                            
                            <?php 
                                $error_message =  Yii::$app->session->getFlash('error');
                                if($error_message!=""){
                            ?>
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                <?php echo $error_message; ?>
                            </div>
                            <?php } ?>
                            
                            <?php $form = ActiveForm::begin(['id' => 'billing-form', 'options' => ['class' => 'nobottommargin']]); ?>
                            

                                <div class="clear"></div>
                                
                                <?php echo $form->field($model, 'card_number')->label('Card Number <span class="required">*</span>'); ?>
                                <?php 
                                    $cards_type = array( 'visa' => 'Visa', 'mastercard' => 'MasterCard', 'amex' => 'American Express', 'discover' => 'Discover');
                                    echo $form->field($model, 'card_type', ['template' => '<div class="col_half">{label}{input}{error}</div>'])->label('Card Type <span class="required">*</span>')->dropDownList($cards_type, ['prompt'=>'Select Card Type'] );
                                ?>
                                <?php echo $form->field($model, 'security_code', ['template' => '<div class="col_half col_last">{label}{input}{error}</div>'])->label('Security Code <span class="required">*</span>'); ?>
                                
                                <?php  
                                    $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December');
                                    echo $form->field($model, 'expiration_month', ['template' => '<div class="col_half">{label}{input}{error}</div>'])->dropDownList($months, ['prompt'=>'Select Expiry Month'] );
                               ?>
                               <?php  
                                    $years_data = array();
                                    for($i = date('Y'); $i <= date('Y')+10; $i++){
                                        $years_data[$i] = $i;
                                    }
                                    echo $form->field($model, 'expiration_year', ['template' => '<div class="col_half col_last">{label}{input}{error}</div>'])->dropDownList($years_data, ['prompt'=>'Select Expiry Year'] );
                               ?>

                                <?php echo Html::submitButton('Make Payment', ['class' => 'button button-3d fright', 'id' => 'billing-form-submit', 'name' => 'billing-form-submit', 'value' => 'Make Payment']); ?>
                                
                            <?php ActiveForm::end(); ?>
                        </div>


                        <h3>International cards</h3>		
                        <p>Accept major international debit or credit cards, including Visa, MasterCard, American Express, Discover, Diners Club and JCB</p>


                        <div class="clear bottommargin"></div>

                    </div>

                    <div class="clear"></div>



                </div>

                </section>

                <!-- #content end -->