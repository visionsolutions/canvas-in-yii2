<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Checkout';
$this->params['breadcrumbs'][] = 'Register New Account';
$this->params['breadcrumbs'][] = 'Pricing';
$this->params['breadcrumbs'][] = $this->title;

?>

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div id="processTabs">
                <ul class="process-steps bottommargin clearfix">
                    <li class="active">
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">1</a>
                        <h5>Review Cart</h5>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">2</a>
                        <h5>Enter Shipping Info</h5>


                    </li>
                    <li>
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">3</a>
                        <h5>Complete Payment</h5>
                    </li>
                    <li>
                        <a href="javascript:void(0);" class="i-circled i-bordered i-alt divcenter">4</a>
                        <h5>Order Complete</h5>
                    </li>
                </ul>
                <div>
                    <div id="ptab1">

                        

                        <div class="table-responsive">

                            <table class="table cart">
                                <thead>
                                    <tr>
                                        <th class="cart-product-name">Product</th>
                                        <th class="cart-product-price">Monthly Price</th>
                                        <th class="cart-product-subtotal">Total</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <tr class="cart_item">
                                        

                                        <td class="cart-product-name">
                                            <?php echo $package['package_name']; ?>
                                        </td>

                                        <td class="cart-product-price">
                                            <span class="amount">€ <?php echo $package['package_price']; ?></span>
                                        </td>

                                        

                                        <td class="cart-product-subtotal">
                                            <span class="amount">€ <?php echo $package['package_price']; ?></span>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>

                        </div>
                        <?php echo Html::a('Checkout &rArr;', ['order/checkout', 'id' => $package['package_id']], ['class' => 'button button-3d nomargin fright tab-linker']); ?>
                        
                    </div>


                    <div class="clear"></div>



                </div>

                </section>

                <!-- #content end -->