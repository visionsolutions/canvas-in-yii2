<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

$this->title = 'Pricing';
//$this->params['breadcrumbs'][] = ['label' => 'Home', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Register New Account';
$this->params['breadcrumbs'][] = $this->title;

?>

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <br >
        <div class="container clearfix">

            
            <div class="pricing bottommargin clearfix">
                <?php
                    if(count($packages) > 0){
                        foreach ($packages as $package){
                            $best_price = '';
                            $button_class = 'btn btn-danger btn-block btn-lg';
                            if($package['package_best_price'] === true){
                                $best_price = 'best-price';
                                $button_class = 'btn btn-danger btn-block btn-lg bgcolor border-color';
                            }
                ?>
                <div class="col-sm-4">
                    <div class="pricing-box <?php echo $best_price; ?>">
                        <div class="pricing-title">
                            <h3><?php echo $package['package_name']; ?></h3>
                            <span><?php echo $package['package_tagline']; ?></span>
                        </div>
                        <div class="pricing-price">
                            <span class="price-unit">&euro;</span><?php echo $package['package_price']; ?><span class="price-tenure">/mo</span>
                        </div>
                        <div class="pricing-features">
                            <ul>
                                <?php foreach ($package['package_option'] as $option){ ?>
                                <li><?php echo $option; ?></li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="pricing-action">
                            <?php echo Html::a('Sign Up', ['order/index', 'id' => $package['package_id']], ['class' => $button_class]); ?>
                            
                        </div>
                    </div>
                </div>
                <?php
                        
                        }
                    }
                ?>

            </div>



        </div>

    </div>

</section>

<!-- #content end -->