<?php
/* @var $this yii\web\View */
$this->title = 'Home - Canvas';
?>
<section id="slider" style="background: url(/canvas/images/landing/landing2.jpg) center; overflow: visible;" data-height-lg="500" data-height-md="500" data-height-sm="600" data-height-xs="600" data-height-xxs="600">
    <div class="container clearfix">

        <form action="#" method="post" role="form" class="landing-wide-form landing-form-overlay dark nobottommargin clearfix">
            <div class="heading-block nobottommargin nobottomborder">
                <h2>Signup for FREE</h2>
                <span>Get 30 Days Unlimited Access</span>
            </div>
            <div class="line" style="margin: 20px 0 30px;"></div>
            <div class="col_full">
                <input type="text" class="form-control input-lg not-dark" value="" placeholder="Your Name*">
            </div>
            <div class="col_full">
                <input type="email" class="form-control input-lg not-dark" value="" placeholder="Your Email*">
            </div>
            <div class="col_full">
                <input type="text" class="form-control input-lg not-dark" value="" placeholder="Phone*">
            </div>
            <div class="col_full">
                <input type="text" class="form-control input-lg not-dark" value="" placeholder="Company*">
            </div>
            <div class="col_full nobottommargin">
                <button class="btn btn-lg btn-danger btn-block nomargin" value="submit" type="submit" style="">START FREE TRIAL</button>
            </div>
        </form>

    </div>
</section>

<!-- Content
============================================= -->
<section id="content" style="overflow: visible;">

    <div class="content-wrap">

        <div class="promo promo-dark promo-full landing-promo header-stick">
            <div class="container clearfix">
                <h3>Detect Defects Before Your Clients Do! <i class="icon-circle-arrow-right" style="position:relative;top:2px;"></i></h3>
                <span>Do you have these defects in Fabrics? </span>


            </div>
        </div>

        <div class="container clearfix">

            <div class="clear bottommargin-lg"></div>

            <div id="section-features" class="heading-block title-center page-section">
                <h2>How to automatically detect defects in any fabric</h2>
                <span>Automated, Saves costs and a helps keep a Great Reputation</span>
            </div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn">
                        <a href="#"><img src="/canvas/images/icons/features/responsive.png" alt="Responsive Layout"></a>
                    </div>
                    <h3>Fabric defect detection</h3>
                    <p>Camera system that automatically detects defects while quality checking fabric. Used for quality checking fabrics before sending to client.</p>
                </div>
            </div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="200">
                        <a href="#"><img src="/canvas/images/icons/features/retina.png" alt="Retina Graphics"></a>
                    </div>
                    <h3>High Quality Camera</h3>
                    <p>Detects 97.99% of all defects. Scans and analyzes fabric in very detailed way.</p>
                </div>
            </div>

            <div class="col_one_third col_last">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="400">
                        <a href="#"><img src="/canvas/images/icons/features/performance.png" alt="Powerful Performance"></a>
                    </div>
                    <h3>Error detection warning</h3>
                    <p>Detects even the smallest scratches and informs user by warning signal.</p>
                </div>
            </div>

            <div class="clear"></div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="600">
                        <a href="#"><img src="/canvas/images/icons/features/flag.png" alt="Responsive Layout"></a>
                    </div>
                    <h3>Connect via Internet</h3>
                    <p>The product is delivered online. Camera installation is required to begin producing results.</p>
                </div>
            </div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="800">
                        <a href="#"><img src="/canvas/images/icons/features/tick.png" alt="Retina Graphics"></a>
                    </div>
                    <h3>Background</h3>
                    <p>Image analyzing & processing technology has been improved over 15 years. It's based on research from International Universities.</p>
                </div>
            </div>

            <div class="col_one_third col_last">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="1000">
                        <a href="#"><img src="/canvas/images/icons/features/tools.png" alt="Powerful Performance"></a>
                    </div>
                    <h3>Control Panel</h3>
                    <p>Yes. You get access to a control panel. You're in control of when and how it's used. </p>
                </div>
            </div>

            <div class="clear"></div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="1200">
                        <a href="#"><img src="/canvas/images/icons/features/map.png" alt="Responsive Layout"></a>
                    </div>
                    <h3>Performance statistics</h3>
                    <p>Powerful tools that allow you to track the most important statistics, speed, quantity, errors etc.</p>
                </div>
            </div>

            <div class="col_one_third">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="1400">
                        <a href="#"><img src="/canvas/images/icons/features/seo.png" alt="Retina Graphics"></a>
                    </div>
                    <h3>Scanned Daily</h3>
                    <p>Daily updates allow users to experience significant improvements in the quality checking process.</p>
                </div>
            </div>

            <div class="col_one_third col_last">
                <div class="feature-box fbox-plain">
                    <div class="fbox-icon" data-animate="bounceIn" data-delay="1600">
                        <a href="#"><img src="/canvas/images/icons/features/support.png" alt="Powerful Performance"></a>
                    </div>
                    <h3>Support</h3>
                    <p>We provide email and phone support as a standard option in European Timezone between 0900 - 1700 (gmt+2).</p>
                </div>
            </div>

            <div class="clear"></div>

            <div class="divider divider-short divider-center"><i class="icon-circle"></i></div>

            <div id="section-pricing" class="heading-block title-center nobottomborder page-section">
                <h2>Pricing Details</h2>
                <span>An All inclusive Pricing Plan that covers your needs</span>
            </div>

            <div class="pricing-box pricing-extended bottommargin clearfix">

                <div class="pricing-desc">
                    <div class="pricing-title">
                        <h3>What's included in the monthly subscription price?</h3>
                    </div>
                    <div class="pricing-features">
                        <ul class="iconlist-color cleafix">
                            <li><i class="icon-desktop"></i> 24x7 Online version</li>
                            <li><i class="icon-eye-open"></i> Camera for scanning</li>
                            <li><i class="icon-dashboard"></i> Advanced Admin Panel</li>
                            <li><i class="icon-magic"></i> Mechanical arm attaching camera</li>
                            <li><i class="icon-user"></i> Support for separate Staff logins</li>
                            <li><i class="icon-file2"></i> Statistics for quality check</li>
                            <li><i class="icon-support"></i> 24x7 Priority Email Support</li>
                        </ul>
                    </div>

                </div>
                You SAVE HALF - INTRODUCTORY OFFER 
                <div class="pricing-action-area">
                    <div class="pricing-meta">
                        You SAVE HALF - INTRODUCTORY OFFER 
                    </div>
                    <div class="pricing-price">
                        <span class="price-unit">&euro;</span>149 <span class="price-tenure">monthly  &nbsp;<strike>&euro;299</strike></span>
                    </div>
                    <div class="pricing-action">
                        <a href="#" class="button button-3d button-large btn-block nomargin">Get Started</a>
                    </div>

                </div>

            </div>



            <div class="clear"></div>

        </div>
        <!-- TESTIMONIALS SECTION -->
        <div class="section">

            <div class="container clearfix">

                <div id="section-testimonials" class="heading-block title-center page-section">
                    <h2>Testimonials</h2>
                    <span>Our All inclusive Pricing Plan that covers you well</span>
                </div>

                <ul class="testimonials-grid grid-3 clearfix">
                    <li>
                        <div class="testimonial">
                            <div class="testi-image">
                                <a href="#"><img src="/canvas/images/testimonials/1.jpg" alt="PolyU researchers, Hong Kong University, China"></a>
                            </div>
                            <div class="testi-content">
                                <p>Said this novel digital analysis and evaluation system will further benefit the textiles and garment industries and sharpen Hong Kong¡¦s competitive edge in the international textiles market by supporting the industries to enhance their product quality control and innovative product design</p>
                                <div class="testi-meta">
                                    PolyU researchers
                                    <span>Hong Kong University, China</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="testimonial">
                            <div class="testi-image">
                                <a href="#"><img src="/canvas/images/testimonials/2.jpg" alt="Jihye Lim and Sungmin Kim, Fiber system engineering, Chonnam National University, Korea"></a>
                            </div>
                            <div class="testi-content">
                                <p>Although it is one of the most important steps in the design and quality control process in fabric production, it has been dependant mainly on human skills with primitive devices such as a magnifying glass and a needle.</p>
                                <div class="testi-meta">
                                    Jihye Lim and Sungmin Kim
                                    <span>Fiber system engineering, Chonnam National University, Korea</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="testimonial">
                            <div class="testi-image">
                                <a href="#"><img src="/canvas/images/testimonials/7.jpg" alt="Customer Testimonails"></a>
                            </div>
                            <div class="testi-content">
                                <p>Fugit officia dolor sed harum excepturi ex iusto magnam asperiores molestiae qui natus obcaecati facere sint amet.</p>
                                <div class="testi-meta">
                                    Department of Fabric and Apparel Science
                                    <span>Google Inc.</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="testimonial">
                            <div class="testi-image">
                                <a href="#"><img src="/canvas/images/testimonials/3.jpg" alt="Customer Testimonails"></a>
                            </div>
                            <div class="testi-content">
                                <p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>
                                <div class="testi-meta">
                                    Steve Jobs
                                    <span>Apple Inc.</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="testimonial">
                            <div class="testi-image">
                                <a href="#"><img src="/canvas/images/testimonials/4.jpg" alt="Customer Testimonails"></a>
                            </div>
                            <div class="testi-content">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, perspiciatis illum totam dolore deleniti labore.</p>
                                <div class="testi-meta">
                                    Jamie Morrison
                                    <span>Adobe Inc.</span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="testimonial">
                            <div class="testi-image">
                                <a href="#"><img src="/canvas/images/testimonials/8.jpg" alt="Customer Testimonails"></a>
                            </div>
                            <div class="testi-content">
                                <p>Porro dolorem saepe reiciendis nihil minus neque. Ducimus rem necessitatibus repellat laborum nemo quod.</p>
                                <div class="testi-meta">
                                    Cyan Ta'eed
                                    <span>Tutsplus</span>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

            </div>

        </div>

        <div class="container clearfix">

            <div id="section-specs" class="heading-block title-center page-section">
                <h2>Product Specifications</h2>
                <span>Complete list of the Tech Specifications for your understanding</span>
            </div>

            <div id="side-navigation">

                <div class="col_one_third">

                    <ul class="sidenav">
                        <li class="ui-tabs-active"><a href="#snav-content1"><i class="icon-magic"></i>Introduction<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content2"><i class="icon-magic"></i>Textile Defect<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content3"><i class="icon-screen"></i>Quality Control<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content4"><i class="icon-tint"></i>Unlimited Color Options<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content5"><i class="icon-gift"></i>Bootstrap 3.1 Compatible<i class="icon-chevron-right"></i></a></li>
                        <li><a href="#snav-content6"><i class="icon-adjust"></i>Light &amp; Dark Scheme<i class="icon-chevron-right"></i></a></li>
                    </ul>

                </div>

                <div class="col_two_third col_last">

                    <div id="snav-content1">
                        <h3>Introduction</h3>
                        <img class="alignright img-responsive" src="/canvas/images/landing/responsive.png" alt="">
                        Visual inspection is an important part of quality
                        control in the textile industry. Efficient automated
                        product inspection is a key factor for the increase of
                        competitiveness of the textile and clothing industry,
                        since it can enable top quality of final products and
                        reduce total cost, through reduction in inspection labor
                        costs, rework labor and scrap material. 
                    </div>

                    <div id="snav-content2">
                        <h3>Textile defect</h3>
                        <img class="alignleft img-responsive" src="http://www.w3schools.com/tags/colormap.gif" alt="">
                        The term
                        ‘textile defect’ covers various types of faults occurring
                        in the fabric resulting from the previous stages of the
                        production. Due to the specific nature of textiles, the
                        defects encountered within textile production must be
                        detected and corrected at early stages of the produc-
                        tion process. In spite of using modern weaving
                        technology, fault detection in many industries still
                        continues to create considerable extra cost
                        <p>
                            1
                            since textile
                            manufacturers  have  to  monitor  continuously  the
                            quality of their products in order to maintain the high
                            quality standards established for the textile industry.
                        </p>
                        <p>
                            2
                            Regarding the type of fabric to be inspected, there are
                            almost  50 different  kinds of  flaws. The quality
                            engineers have to deal with an extensive variety of
                            defects either due to mechanical malfunction of the
                            loom, or due to low-quality fibers and spreads. At
                            present, the quality assessment procedures are gene-
                            rally performed manually by expert quality engineers
                            and technicians as indicated in Figure 1. However, the
                            detection and classification of these defects are time-
                            consuming and tiring procedures. Moreover, the low
                            quality control speed when compared to the produc-
                            tion speed reveals the bottleneck in the workflow.
                        </p>
                    </div>

                    <div id="snav-content3">
                        <h3>Quality Control</h3>
                        <img class="alignleft img-responsive" src="http://www.w3schools.com/tags/colormap.gif" alt="colormap">
                        To increase accuracy, attempts are made to
                        replace the traditional human inspection by automated
                        visual systems, which employ camera nodes and
                        image-processing  routines.  Image  acquisition  and
                        automatic evaluation may form the basis for a system
                        that will ensure a very high degree of fabric quality
                        control. The main difficulty with computer-based
                        inspection units in fabrics is the great diversity of
                        their types and defects.
                    </div>

                    <div id="snav-content4">
                        <img class="alignleft img-responsive" src="/canvas/images/landing/bootstrap.png" alt="">
                        <h3>Bootstrap v3.2.0 Compatiable</h3>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, nostrum, dolores id quo nam repudiandae ad culpa architecto minima nemo eaque soluta nulla laborum neque provident saepe facilis expedita numquam quas alias in perferendis accusamus ipsam blanditiis sit voluptatem temporibus vero error veritatis repellat eos reiciendis repellendus quam. Officia dicta ipsam nostrum aperiam. Dolor, expedita enim modi nostrum commodi sint architecto aliquam aut mollitia repellendus deserunt quaerat aspernatur aperiam voluptatibus consequatur rerum consequuntur.
                    </div>

                    <div id="snav-content5">
                        <h3>Light &amp; Dark Scheme Available</h3>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, temporibus, maxime, laudantium quidem sapiente deserunt error rerum illum explicabo voluptate velit tempora cupiditate reprehenderit consequuntur nemo in et blanditiis soluta tempore perspiciatis at atque excepturi culpa facere sequi impedit cumque illo molestias saepe eveniet ducimus fugiat reiciendis unde. Modi, at laboriosam ex velit commodi officiis! Neque, consequatur, modi, nulla, voluptatem quibusdam incidunt minus dolores repellat nihil consectetur ducimus aliquid. Eaque, tempora voluptatum accusantium expedita obcaecati magnam voluptates consequatur ut harum rem dolor id error. Officia, repudiandae, eos, quibusdam porro eius esse cupiditate non fugit dignissimos delectus et tempora sequi fugiat quo voluptatem temporibus vel obcaecati? Laboriosam, quis obcaecati quas veniam repellendus officiis et quos velit id natus mollitia dacilis ipsum et perspiciatis officia iste cupiditate ducimus nisi consequuntur excepturi dolorum. Sint, architecto, cumque facere officia harum dicta perferendis inventore excepturi sequi explicabo provident omnis dolore quasi fugit molestiae atque id consectetur reprehenderit laborum beatae consequatur similique.
                    </div>

                    <div id="snav-content6">
                        <h3>Light &amp; Dark Scheme Available</h3>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quas, temporibus, maxime, laudantium quidem sapiente deserunt error rerum illum explicabo voluptate velit tempora cupiditate reprehenderit consequuntur nemo in et blanditiis soluta tempore perspiciatis at atque excepturi culpa facere sequi impedit cumque illo molestias saepe eveniet ducimus fugiat reiciendis unde. Modi, at laboriosam ex velit commodi officiis! Neque, consequatur, modi, nulla, voluptatem quibusdam incidunt minus dolores repellat nihil consectetur ducimus aliquid. Eaque, tempora voluptatum accusantium expedita obcaecati magnam voluptates consequatur ut harum rem dolor id error. Officia, repudiandae, eos, quibusdam porro eius esse cupiditate non fugit dignissimos delectus et tempora sequi fugiat quo voluptatem temporibus vel obcaecati? Laboriosam, quis obcaecati quas veniam repellendus officiis et quos velit id natus mollitia dacilis ipsum et perspiciatis officia iste cupiditate ducimus nisi consequuntur excepturi dolorum. Sint, architecto, cumque facere officia harum dicta perferendis inventore excepturi sequi explicabo provident omnis dolore quasi fugit molestiae atque id consectetur reprehenderit laborum beatae consequatur similique.
                    </div>

                </div>

            </div>
            <?php
            $js_code = '$("#side-navigation").tabs({show: {effect: "fade", duration: 400}});';
            $this->registerJs($js_code);
            ?>


        </div>

        <div class="section footer-stick">

            <div class="container clearfix">

                <div id="section-buy" class="heading-block title-center nobottomborder page-section">
                    <h2>Enough? Start Building!</h2>
                    <span>Now that you have read all the Tid-Bits, so start with a plan</span>
                </div>

                <div class="center">

                    <a href="#" data-animate="tada" class="button button-3d button-teal button-xlarge nobottommargin"><i class="icon-star3"></i>Start your FREE Trial</a> - OR - <a href="#" data-scrollto="#section-pricing" class="button button-3d button-red button-xlarge nobottommargin"><i class="icon-user2"></i>Sign Up for a Subscription</a>

                </div>

            </div>

        </div>

    </div>

</section><!-- #content end -->