<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <p>
                <b><?= nl2br(Html::encode($message)) ?></b>
                <br>
                The above error occurred while the Web server was processing your request.
                <br>
                Please contact us if you think this is a server error. Thank you.
            </p>
            
        </div>
    </div>
</section>

