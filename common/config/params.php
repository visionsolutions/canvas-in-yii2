<?php
$packages[0]['package_id'] = 1;
$packages[0]['package_name'] = 'Professional';
$packages[0]['package_tagline'] = 'High Speed Scanning';
$packages[0]['package_price'] = 499;
$packages[0]['package_best_price'] = false;
$packages[0]['package_option'][] = '<strong>Full</strong> Access';
$packages[0]['package_option'][] = '<i class="icon-leaf"></i> <strong>Unlimited</strong> Scanning';
$packages[0]['package_option'][] = '<strong>Max Scan Speed</strong> /4M Per Sec';
$packages[0]['package_option'][] = '<strong>5</strong> User Accounts';
$packages[0]['package_option'][] = '<strong>Professional</strong> Reports';
$packages[0]['package_option'][] = '<strong>Cameratype</strong> Advanced PRO';
$packages[0]['package_option'][] = '<strong>Access</strong> Online version';
$packages[0]['package_option'][] = 'Phone &amp; Email Support';

$packages[1]['package_id'] = 2;
$packages[1]['package_name'] = 'Business';
$packages[1]['package_tagline'] = 'Most Popular';
$packages[1]['package_price'] = 149;
$packages[1]['package_best_price'] = true;
$packages[1]['package_option'][] = '<strong>Full</strong> Access';
$packages[1]['package_option'][] = '<i class="icon-leaf"></i> <strong>8hrs</strong> Scanning /day';
$packages[1]['package_option'][] = '<strong>Max Scan Speed</strong> /2M Per Sec';
$packages[1]['package_option'][] = '<strong>3</strong> User Accounts';
$packages[1]['package_option'][] = '<strong>Professional</strong> Reports';
$packages[1]['package_option'][] = '<strong>Cameratype</strong> Webcamera PRO';
$packages[1]['package_option'][] = '<strong>Access</strong> Online version';
$packages[1]['package_option'][] = 'Phone &amp; Email Support';

$packages[2]['package_id'] = 3;
$packages[2]['package_name'] = 'SIGNUP FOR FREE';
$packages[2]['package_tagline'] = 'Get 30 Days Unlimited Access';
$packages[2]['package_price'] = 0;
$packages[2]['package_best_price'] = false;
$packages[2]['package_option'][] = '<strong>Full</strong> Access';
$packages[2]['package_option'][] = '<i class="icon-leaf"></i> <strong>8hrs</strong> Scanning /day';
$packages[2]['package_option'][] = '<strong>Max Scan Speed</strong> /2M Per Sec';
$packages[2]['package_option'][] = '<strong> AdminAccount only </strong>';
$packages[2]['package_option'][] = '<strong>Professional</strong> Reports';
$packages[2]['package_option'][] = '<strong>Cameratype</strong> Webcamera PRO';
$packages[2]['package_option'][] = '<strong>Access</strong> Online version';
$packages[2]['package_option'][] = 'Phone &amp; Email Support';


$products_access[3] = "User (Employee)(People who will work on system)";
$products_access[2] = "Billing (Accountants)";
$products_access[1] = "Primary (Administrator)(Owners, CEO's)";

return [
    'adminEmail' => 'tanveerhussain.nxb@gmail.com',
    'supportEmail' => 'tanveerhussain.nxb@gmail.com',
    'application_name' => 'Canvas',
    'user.passwordResetTokenExpire' => 3600,
    'packages' => $packages,
    'products_access' => $products_access,
];
