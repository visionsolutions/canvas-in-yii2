<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$loginLink = Yii::$app->urlManager->createAbsoluteUrl(['user/login']);
?>
<div class="password-reset">
    <p>Hello <?php echo Html::encode($user['name']); ?>,</p>

    <p>Following is your login information:</p>
    
    <p>Username: <?php echo Html::encode($user['username']); ?></p>
    <p>Password: <?php echo Html::encode($user['custom_password']); ?></p>

    <p><?php echo Html::a(Html::encode($loginLink), $loginLink); ?></p>
</div>
