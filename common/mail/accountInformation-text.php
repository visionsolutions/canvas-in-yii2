<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$loginLink = Yii::$app->urlManager->createAbsoluteUrl(['user/login']);
?>
Hello <?php echo $user['name']; ?>,

Following is your login information:

Username: <?php echo $user['username']; ?>

Password: <?php echo $user['custom_password']; ?>

<?php echo $loginLink; ?>
