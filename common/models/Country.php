<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Country form
 */
class Country extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'country';
    }

}
